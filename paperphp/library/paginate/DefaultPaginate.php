<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\paginate;


use paper\Paginate;

class DefaultPaginate extends Paginate
{


    protected function buildPage()
    {
        $page = '<ul class="paginate">';
        $page .= $this->getPagePrev();
        $page .= $this->getBefore();
        $page .= $this->getMiddle();
        $page .= $this->getAfter();
        $page .= $this->getPageNext();
        $page .= '</ul>';
        return $page;
    }

    private function getBefore()
    {
        $html = '';
        if ($this->showBeforePage) {
            for ($i = $this->beforeStartPage; $i <= $this->beforeEndPage; $i++) {
                $html .= "<li><a href='{$this->url($i)}'>{$i}</a></li>";
            }
            $html .= "<li><span>...</span></li>";
        }
        return $html;
    }

    private function getAfter()
    {
        $html = '';
        if ($this->showAfterPage) {
            $html .= "<li><span>...</span></li>";
            for ($i = $this->afterStartPage; $i <= $this->afterEndPage; $i++) {
                $html .= "<li><a href='{$this->url($i)}'>{$i}</a></li>";
            }
        }
        return $html;
    }

    private function getMiddle()
    {
        $html = '';
        if ($this->showMiddlePage) {
            for ($i = $this->middleStartPage; $i <= $this->middleEndPage; $i++) {
                if($this->current == $i){
                    $html .= "<li class='active'><span>{$i}</span></li>";
                }else{
                    $html .= "<li><a href='{$this->url($i)}'>{$i}</a></li>";
                }
            }
        }
        return $html;
    }

    private function getPagePrev()
    {
        if($this->current > 1){
            return '<li class="page-prev"><a href="'.$this->url(($this->current)-1).'">上一页</a></li>';
        }
        return '<li class="page-prev"><span>上一页</span></li>';
    }
    private function getPageNext()
    {
        if($this->current < $this->pages){
            return '<li class="page-prev"><a href="'.$this->url(($this->current)+1).'">下一页</a></li>';
        }
        return '<li class="page-prev"><span>下一页</span></li>';
    }
}