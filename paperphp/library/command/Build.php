<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/7/8
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\command;

class Build
{

    /**
     * 创建模块
     * @param $name
     */
    private function createModule($name)
    {
        $path = ROOT . DIRECTORY_SEPARATOR . APP . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $name;
        if (!is_dir($path)) {
            mkdir($path, '0777');
            mkdir($path . DIRECTORY_SEPARATOR . 'controllers', '0777');
            mkdir($path . DIRECTORY_SEPARATOR . 'views', '0777');
            mkdir($path . DIRECTORY_SEPARATOR . 'models', '0777');
            echo '已经创建模块' . $name;
        } else {
            echo '模块已经存在';
        }
    }

    public function exec($params)
    {
        if (isset($params["module"])) {
            $this->createModule($params["module"]);
        }
    }
}