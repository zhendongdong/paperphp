<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\response;

use paper\http\Response;

class View extends Response
{

    protected $contentType = 'text/html';

    /**
     * 处理数据
     * @access protected
     * @param \paper\View $data 要处理的数据
     * @return false|string
     * @throws \Exception
     */
    protected function output($data)
    {
        return $data->fetch();
    }

}