<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\response;

use paper\http\Response;

class Exception extends Response
{

    protected $contentType = 'text/html';

    /**
     * 处理数据
     * @access protected
     * @param mixed $data 要处理的数据
     * @return mixed
     * @throws \Exception
     */
    protected function output($data)
    {
        return $data;
    }

    /**
     * @param \Closure $data
     * @throws \paper\Exception
     */
    protected function sendData($data)
    {
        $data();
    }

}