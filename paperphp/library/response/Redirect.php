<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/15
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\response;


use paper\http\Response;

class Redirect extends Response
{

    protected function sendData($data)
    {
        header("location:" . $data);
    }

}