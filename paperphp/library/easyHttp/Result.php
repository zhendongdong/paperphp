<?php
/**
 * Author: DongGua
 * E-mail zhendongdong@foxmail.con
 * Date: 2018/6/21
 * Time: 0:21
 */

namespace paper\easyHttp;


class Result
{
    private $code = null;
    private $data = '';

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    public function fetchJson()
    {
        return json_decode($this->data, true);
    }
}