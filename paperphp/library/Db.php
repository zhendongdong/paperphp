<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper;


use paper\pdo\Query;
use paper\support\Model;

/**
 * @method static Query name($table, $prefix = null)
 * @method static Query table($table)
 * @method static Query model(Model $model)
 * @method static startTrans()
 * @method static rollback()
 * @method static commit()
 * @method static query($sql, $data = [], $one = true)
 */
class Db
{

    private static $config = [];

    private static function connect()
    {
        $config = App::getInstance()->config->database();
        return new Query($config);
    }

    /**
     * @param $name
     * @param $arguments
     * @return Query
     */
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([self::connect(), $name], $arguments);
    }

}

