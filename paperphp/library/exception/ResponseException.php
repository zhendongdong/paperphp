<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\exception;


use paper\http\Response;
use RuntimeException;
use Throwable;

class ResponseException extends RuntimeException
{
    private $response;


    public function __construct(Response $response = null,$message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        if($response){
            $this->response = $response;
        }
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

}