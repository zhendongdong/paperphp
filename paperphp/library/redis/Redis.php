<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/7/2
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\redis;

use paper\App;
use paper\server\Server;
use Redis as RedisLibrary;

class Redis implements Server
{
    private static $connect = null;

    public function __construct()
    {
        if (is_null(self::$connect)) {
            $redis = new RedisLibrary();
            $redis->open('127.0.0.1', 6379, 1);
            self::$connect = $redis;
        }
    }

    public function connect(){
        return self::$connect;
    }
    /**
     * 系统服务注册
     * @param App $app
     */
    public static function _make(App $app)
    {
        $app->singleton(self::class);
    }
}