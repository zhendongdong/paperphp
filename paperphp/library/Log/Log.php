<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/16
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\log;


use paper\App;

class Log
{
    private $sqlLog = [];
    private $app;
    private $log;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function error($message)
    {
        $this->log['error'][] = [$message, time()];
    }

    public function sql($str, $data)
    {
        if ($this->app->config->app('debug')) {
            $this->sqlLog[] = ['sql' => $str, 'data' => $data];
        }
    }

    /**
     * @return array
     */
    public function getSqlLog(): array
    {
        return $this->sqlLog;
    }

}