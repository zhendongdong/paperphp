<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\exception;


class ClassNotFoundException extends \RuntimeException
{
    protected $class;

    public function __construct($message, $class = '')
    {
        $this->message = $message;
        $this->class   = $class;
        parent::__construct($message, $code = 0);
    }

    /**
     * 获取类名
     * @access public
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }
}