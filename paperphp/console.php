<?php
/**
 * Created by Paperphp
 * User: 22071
 * Date: 2019/6/17
 * Email: <zhendongdong@foxmail.com>
 */

use paper\Console;

require __DIR__ . '/paperphp.php';

try {
    (new Console())->exec($argv);
} catch (Exception $e) {
    echo $e->getMessage();
}