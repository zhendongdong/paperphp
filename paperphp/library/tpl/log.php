<?php
/**
 * @var integer $runTime
 */

use paper\App;

$Log  = App::getInstance()->log;
$logs = $Log->getSqlLog();

?>
<div style="height: 120px"></div>
<div class="" id="log"
     style="position:fixed;background:#efefef;bottom: 0;width: 100%;padding: 10px 20px;height: 120px;box-sizing: border-box;overflow-y: auto">
    <ul style="list-style: none;padding: 0;margin: 0">
        <?php
        foreach ($logs as $sql) {
            echo '<li>' . $sql['sql'] . '</li>';
        }
        ?>
    </ul>
    运行时间：<?= round($runTime['use_time'], 4) ?> 秒&nbsp;使用内存：<?= round($runTime['memory'] / 1024, 4) ?>KB
</div>
