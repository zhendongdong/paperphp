<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\providers;


use paper\App;
use paper\support\ServiceProvider;
use paper\Url;

class UrlServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(Url::class, function (App $app) {
            return new Url($app);
        });
    }
}