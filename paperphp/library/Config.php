<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper;


use paper\server\Server;

/**
 * @method app($item = null)
 * @method database($item = null)
 * @method paginate($item = null)
 * @method session($item = null)
 */
class Config implements Server
{
    private $config = [];

    /**
     * 读取配置文件
     * @param string $configSource
     * @param null $item
     * @return mixed|null
     */
    public function get($configSource = 'app', $item = null)
    {
        $configSource = strtolower($configSource);
        if (!isset($this->config[$configSource])) {
            $this->loadConfig($configSource);
        }
        if (!$item)
            return $this->config[$configSource];
        $name  = explode(".", $item);
        $value = $this->config[$configSource];
        foreach ($name as $key) {
            if (!isset($value[$key]))
                return null;
            $value = $value[$key];
        }
        return $value;
    }

    /**
     * 加载配置文件
     * @param $configSource
     *
     */
    private function loadConfig($configSource)
    {
        $this->config[$configSource] = require ROOT . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . $configSource . '.php';
    }

    public function __call($name, $value)
    {
        array_unshift($value, $name);
        return call_user_func_array([$this, 'get'], $value);
    }
}