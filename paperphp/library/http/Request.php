<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/25
 * Email: <zhendongdong@foxmail.com>
 */


namespace paper\http;


use paper\App;
use paper\session\Session;
use paper\support\Upload;

class Request
{
    private $path;
    private $app;
    private $post;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->prepareRequest();

        $path = $this->pathinfo();
        if (!$path) {
            $this->path = '/';
        } else {
            if ($suffix = strrchr($path, ".")) {
                $path = str_replace($suffix, "", $path);
            }
            $this->path = $path;
        }
    }


    /**
     *
     */
    private function prepareRequest()
    {
        // 如果Content-Type的类型为application/json则获取原 数据并且转JSON为数组
        if (strtolower($this->contentType()) == ' application/json') {
            $this->post = json_encode($this->raw());
        } else {
            $this->post = $_POST;
        }
    }

    /**
     * 获取原始数据
     * @return false|string
     */
    public function raw()
    {
        return file_get_contents("php://input");
    }

    /**
     *
     * 获取请求中的Session
     * @return Session
     */
    public function session()
    {
        return $this->app->session;
    }


    public function path()
    {
        return $this->path;
    }

    public function domain()
    {
        return $this->server("HTTP_HOST");
    }

    public function router()
    {
        return $this->app->router;
    }


    /**
     * 当前URL的访问后缀
     * @access public
     * @return string
     */
    public function ext()
    {
        return pathinfo($this->pathinfo(), PATHINFO_EXTENSION);
    }

    /**
     * 是否为cgi
     * @access public
     * @return bool
     */
    public function isCgi()
    {
        return strpos(PHP_SAPI, 'cgi') === 0;
    }

    public function contentType()
    {
        return $this->header("Content-Type");
    }

    /**
     * 获取header请求头
     * @param null $key
     * @return array|string
     */
    public function header($key = null)
    {
        if (!$key) {
            $header = [];
            foreach ($_SERVER as $item => $value) {
                if ('HTTP_' != substr($item, 0, 5)) {
                    continue;
                }
                $headerKey          = substr($item, 5);
                $header[$headerKey] = $value;
            }
            return $header;
        }
        $headerKey = 'HTTP_' . strtoupper($key);
        return $this->server($headerKey);
    }


    /**
     * 获取当前控制器
     * @param bool $toCamelCase
     * @return string
     */
    public function controller($toCamelCase = true)
    {
        return $toCamelCase ? $this->app->router->controller : $this->app->router->_controller;
    }

    /**
     * @param bool $toCamelCase
     * @return string
     */
    public function action($toCamelCase = true)
    {
        return $toCamelCase ? $this->app->router->method : $this->app->router->_method;
    }

    /**
     * 获取GET方法中的参数
     * @param null $name
     * @param null $default
     * @return mixed|null
     */
    public function get($name = null, $default = null)
    {
        if (!$name) {
            return $_GET;
        }
        if (!isset($_GET[$name]) || !$_GET[$name]) {
            return $default;
        }
        return isset($_GET[$name]) && $_GET[$name] ? $_GET[$name] : $default;
    }

    /**
     * 获取POST方法中的参数
     * @param null $name
     * @param null $default
     * @return mixed|null
     */
    public function post($name = null, $default = null)
    {
        if (!$name) {
            return $_POST;
        }
        return $this->post[$name] ?? $default;
    }

    /*
     * 判断是否是ajax请求
     * */
    public function isAjax()
    {
        return strtolower($this->server('HTTP_X_REQUESTED_WITH')) == "xmlhttprequest";
    }

    /*
     * 判断是否是post请求
     * */
    public function isPost()
    {
        return strtolower($this->server('REQUEST_METHOD')) == "post";
    }

    public function method()
    {
        return $this->server('REQUEST_METHOD');
    }

    public function file($field)
    {
        if (!isset($_FILES[$field])) {
            return false;
        }
        return new Upload($_FILES[$field]);
    }

    public function server($name = null)
    {
        if (!$name)
            return $_SERVER;
        if (isset($_SERVER[$name])) {
            return $_SERVER[$name];
        }
        return '';
    }

    private function pathinfo()
    {
        return $this->server('PATH_INFO');
    }

    public function referer()
    {
        return $this->server("HTTP_REFERER");
    }


    public function set()
    {

    }

}