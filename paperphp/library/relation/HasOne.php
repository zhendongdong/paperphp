<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/15
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\relation;


use paper\Relation;

class HasOne extends Relation
{

    /**
     * @param $result
     * @return array|bool|mixed
     * @throws \Exception
     */
    public function prepareRelation(&$result)
    {
        if (!$result)
            return [];
        $keys = array_column($result, $this->foreignKey);
        $list = $this->query->where($this->localKey, 'in', $keys)->where($this->where)->index($this->localKey)->select();
        return $list;
    }

    public function getRelationKey($localKey, $foreignKey)
    {
        $this->localKey   = $localKey ?: $this->model->tabName() . '_id';
        $this->foreignKey = $foreignKey ?: $this->relationModel->getPk();
    }

    /**
     * @return \paper\pdo\Query
     */
    protected function constraint()
    {
        $v = $this->model->getData($this->foreignKey);
        return $this->query->where([$this->localKey => $v])->where($this->where);
    }

    /**
     * @return array|bool|mixed|void
     * @throws \Exception
     */
    public function getData()
    {
        return $this->constraint()->find();
    }
}