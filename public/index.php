<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/20
 * Email: <zhendongdong@foxmail.com>
 * link: http://www.paper.php
 */

use paper\App;
use paper\Config;
use paper\http\Response;
use paper\Kernel;

error_reporting(E_ALL);

require __DIR__ . '/../paperphp/paperphp.php';

define("APP", "app");

$app = new App(); 

$app->singleton(Config::class);
$app->singleton(Kernel::class);

/** @var Response $response * */
$response = $app->make(Kernel::class)->handler();
$response->send();
