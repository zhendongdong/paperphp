<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\providers;


use paper\App;
use paper\support\ServiceProvider;
use paper\tplLibs\Engine;
use paper\View;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Engine::class, function (App $app) {
            return new Engine();
        });
        $this->app->singleton(View::class, function (App $app) {
            return new View($app);
        });
    }
}