<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\providers;


use paper\App;
use paper\log\Log;
use paper\support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Log::class, function (App $app) {
            return new Log($app);
        });
    }
}