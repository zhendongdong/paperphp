<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\support;


use paper\App;

class ServiceProvider
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }
}