<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper;


class Cookie
{
    public function __construct(App $app)
    {
    }

    public  function set($name, $value)
    {
        $_COOKIE[$name] = $value;
    }

    public  function get($name)
    {
        return $_COOKIE[$name];
    }

}