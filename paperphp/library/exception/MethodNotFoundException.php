<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\exception;


use Throwable;

class MethodNotFoundException extends \RuntimeException
{
    public function __construct($message = "", $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}