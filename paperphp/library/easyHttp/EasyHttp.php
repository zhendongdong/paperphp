<?php
/**
 * Author: DongGua
 * E-mail zhendongdong@foxmail.con
 * Date: 2018/6/21
 * Time: 0:16
 */

namespace paper\easyHttp;


class EasyHttp
{


    private $ssl = false;
    private $url = [];


    public static function request($url, $post_data, $option = [])
    {
        $options = [
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [],
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 0,
        ];
        if ($options) {
            $options = array_merge($options, $option);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($post_data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        foreach ($options as $key => $value) {
            curl_setopt($ch, $key, $value);
        }

        $return_content = curl_exec($ch);
        $return_code    = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = new Result();
        $result->setCode($return_code);
        $result->setData($return_content);
        return $result;
    }

    public function ssl($ssl)
    {
        $this->ssl = $ssl;
    }
}