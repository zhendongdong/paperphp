<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/7/2
 * Email: <zhendongdong@foxmail.com>
 */

return [
    //是否使session自动启动
    'session_auto_start'     => true,
    //session名字
    'session_name'           => 'SESSION_ID',
];