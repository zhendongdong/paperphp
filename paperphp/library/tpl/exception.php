<?php
/** @var Exception $exception */
/** @var Exception $message */
/** @var integer $line */
/** @var integer $start */
/** @var string $file */
/** @var array $nl */
/** @var array $trace */
/** @var string $subMessage */
?>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        html, body {
            font-family: "微软雅黑";
        }
        .code{
            padding: 15px;
            font-size: 14px;
            line-height: 25px;
            margin: 20px 0;
            background: #f8f8f8;
        }
        .code ul{
            padding: 0;
            margin: 0;
            list-style: none;
        }
        .code ul li.active{
            background: #ffffff;
            font-weight: bolder;
            color: red;
        }
        .trace{
            list-style: none;
            padding: 15px;
            margin: 0;
            background: #f8f8f8;
            font-size: 14px;
            line-height: 25px;
        }
    </style>
</head>
<body>
<h1><?= $message ?></h1>
<h3><?= "{$file} line :{$line}" ?></h3>
<?php
if($subMessage){
    echo "<div class='code'>{$subMessage}</div>";
}
?>
<div class="code"><ul><?php
        $i = $start+1;
        foreach ($nl as $l){
            if($i == $line){
                $pre = 'active';
            }else{
                $pre = '';
            }
            ?>
            <li class="<?=$pre?>"><span><?=$i?></span><?=$l?></li>
        <?php
        $i++;
        } ?></ul>
</div>
<ul class="trace">
    <?php
    if ($trace) {
        foreach ($trace as $item) {
            echo "<li>";
                var_dump($item);
            echo "</li>";
        }
    }
    ?>
</ul>

</body>
</html>