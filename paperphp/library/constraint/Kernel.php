<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\constraint;


interface Kernel
{
    public function handler();

    public function terminate();

    public function getApplication();
}