<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\providers;

use paper\App;
use paper\Cookie;
use paper\Dispatch;
use paper\http\Request;
use paper\Middleware;
use paper\routing\Router;
use paper\session\Session;
use paper\support\ServiceProvider;

class HttpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Request::class, function (App $app) {
            return new Request($app);
        });
        $this->app->singleton(Cookie::class, function (App $app) {
            return new Cookie($app);
        });
        $this->app->singleton(Router::class, function (App $app) {
            return new Router($app);
        });
        $this->app->singleton(Dispatch::class, function (App $app) {
            return new Dispatch($app);
        });
        $this->app->singleton(Middleware::class, function (App $app) {
            return new Middleware($app);
        });
        $this->app->singleton(Session::class, function (App $app) {
            return new Session($app);
        });
    }
}