<?php
declare(strict_types = 1);

namespace paper;

//PHP版本判断

if (version_compare(PHP_VERSION, '7.1.0') == -1) {
    die("PHP version must >= 7.1.0,your PHP version is " . PHP_VERSION);
}

define("IN_APP", true);
define("COMPOSER", true);
define("ROOT", dirname(__DIR__));
define("PAPERPHP", __DIR__);

require PAPERPHP . '/library/Loader.php';
require PAPERPHP . '/library/helper/helper.php';

Loader::register();