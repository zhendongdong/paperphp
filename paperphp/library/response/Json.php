<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\response;


use paper\http\Response;

class Json extends Response
{
    // 输出参数
    protected $options = [
        'json_encode_param' => JSON_UNESCAPED_UNICODE,
    ];

    protected $contentType = 'application/json';

    /**
     * 处理数据
     * @access protected
     * @param mixed $data 要处理的数据
     * @return mixed
     * @throws \Exception
     */
    protected function output($data)
    {

        // 返回JSON数据格式到客户端 包含状态信息
        $data = json_encode($data, $this->options['json_encode_param']);

        if (false === $data) {
            throw new \InvalidArgumentException(json_last_error_msg());
        }

        return $data;

    }

}