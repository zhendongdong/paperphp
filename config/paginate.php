<?php

use paper\paginate\DefaultPaginate;

return [
    'auto_params' => true,
    'params'      => false,
    'render'      => DefaultPaginate::class,
];