<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/26
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\constraint;

use Closure;
use paper\http\Request;

interface Middleware
{
    public function handle(Request $request, Closure $next);
}