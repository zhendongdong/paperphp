<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/6/25
 * Email: <zhendongdong@foxmail.com>
 */


namespace paper;


use paper\exception\ResponseException;
use paper\http\Response;

class Controller
{

    protected $request = null;
    private $view = null;
    protected $app = null;

    /**
     * Controller constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $app->request;
        $this->view    = $app->view;

        $this->view->assign("_P", [
            'request' => $this->app->request,
        ]);
        //执行初始化函数
        $this->initialize();
        $this->boot();
    }

    protected function boot()
    {

    }

    /**
     * 控制器初始化
     */
    protected function initialize()
    {

    }

    protected function assign($var, $value)
    {
        $this->view->assign($var, $value);
    }

    /**
     * @param $msg
     * @param string $url
     * @throws ResponseException
     */
    protected function success($msg, $url = null)
    {
        $url = $url ?: $this->request->server("HTTP_REFERER");
        $this->view->assign("type", "success");
        $this->view->assign("msg", $msg);
        $this->view->assign("url", $url);
        $this->view->view("public/tips");
        $response  = Response::create($this->view, 200, 'view');
        $exception = new ResponseException();
        $exception->setResponse($response);
        throw $exception;
    }

    /**
     * @param $msg
     * @param string $url
     * @throws ResponseException
     */
    protected function error($msg, $url = null)
    {
        $url = $url ?: $this->request->server("HTTP_REFERER");
        $this->view->assign("type", "error");
        $this->view->assign("msg", $msg);
        $this->view->assign("url", $url);
        $this->view->view("public/tips");
        $response  = Response::create($this->view, 200, 'view');
        $exception = new ResponseException();
        $exception->setResponse($response);
        throw $exception;
    }


    protected function view($template = ''): View
    {
        $this->view->view($template);
        return $this->view;
    }

    /**
     * @param array $data
     * @param int $code
     * @param string $msg
     * @param string $type
     * @throws ResponseException
     */
    protected function result($data = [], $code = 0, $msg = '', $type = 'json')
    {
        $result["code"] = $code;
        $result["data"] = $data;
        $result["msg"]  = $msg;
        $result["time"] = time();

        $response          = Response::create($result, 200, $type, []);
        $responseException = new ResponseException();
        $responseException->setResponse($response);
        throw $responseException;
    }

    /**
     * @param string $url
     * @param int $code
     * @throws ResponseException
     */
    protected function redirect($url = '', $code = 302)
    {
        $response          = Response::create($url, $code, 'redirect', []);
        $responseException = new ResponseException();
        $responseException->setResponse($response);
        throw $responseException;
    }
}