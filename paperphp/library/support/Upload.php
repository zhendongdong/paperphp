<?php
/**
 * Created by Paperphp
 * User: 22071
 * Date: 2019/6/18
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\support;


class Upload
{

    private $file;
    private $tmp_file_path;
    private $pathinfo = [];
    private $allow_ext = [];
    private $extension = '';
    private $allow_size = 1024 * 200;
    private $size = 0;
    private $type = '';
    private $name = '';
    private $save_path = '';
    private $fileName = '';
    private $err_code = 5;
    private $err_msg = [
        0 => '文件上传成功',
        1 => '上传的文件超过了 php.ini 中 upload_max_filesize选项限制的值',//UPLOAD_ERR_INI_SIZE 系统错误
        2 => '上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值',//UPLOAD_ERR_FORM_SIZE 系统错误
        3 => '文件只有部分被上传',//UPLOAD_ERR_PARTIAL 系统错误
        4 => '没有文件被上传',//UPLOAD_ERR_NO_FILE 系统错误
        5 => '找不到临时文件',//UPLOAD_ERR_NO_TMP_DIR 系统错误
        6 => '文件写入失败',  //UPLOAD_ERR_CANT_WRITE 系统错误
        1000 => '文件大小超过限制',  //
        1001 => '文件类型不允许',  //
    ];

    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getErrCode(): int
    {
        return $this->err_code;
    }

    /**
     * @return string
     */
    public function getErrMsg(): string
    {
        return $this->err_msg[$this->err_code];
    }

    /**
     * @param int $err_code
     */
    public function setErrCode($err_code = 5): void
    {
        $this->err_code = $err_code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return mixed
     */
    public function getTmpFilePath()
    {
        return $this->tmp_file_path;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function validate($rule)
    {
        if (!$this->file) {
            $this->setErrCode(5);
            return false;
        }
        if ($this->file["error"] > 0) {
            $this->setErrCode($this->file["error"]);
            return false;
        }
        $this->pathinfo  = pathinfo($this->file['name']);
        $this->extension = strtolower($this->pathinfo['extension']);
        if (($rule['ext']) && is_string($rule['ext'])) {
            $rule['ext'] = explode(",", $rule['ext']);
        }
        foreach ($rule['ext'] as $ext) {
            $this->allow_ext[] = trim(strtolower($ext));
        }
        if (isset($rule['size'])) {
            $this->allow_size = $rule['size'];
        }

        if ($this->file["size"] > $this->allow_size) {
            $this->setErrCode(1000);
            return false;
        }

        if (!in_array($this->extension, $this->allow_ext)) {
            $this->setErrCode(1001);
            return false;
        }


        $this->size          = $this->file['size'];
        $this->type          = $this->file['type'];
        $this->tmp_file_path = $this->file['tmp_name'];
        $this->name          = $this->file['name'];

        return $this;

    }

    public function save($path)
    {
        return $this->move($path);
    }

    private function name()
    {

    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }


    /**
     * @param $path
     * @return mixed
     */
    public function move($path = null)
    {
        if ($path instanceof \Closure) {
            return $path($this->tmp_file_path);
        }
        $path            = $path ?: 'upload';
        $this->save_path = $path . '/' . date('Ymd');
        if (!is_dir($this->save_path)) {
            @mkdir($this->save_path, 0777, true);
        }
        $this->fileName = $this->save_path . '/' . md5($this->name . rand(0, 1000000)) . '.' . $this->extension;
        return move_uploaded_file($this->file["tmp_name"], $this->fileName);
    }
}