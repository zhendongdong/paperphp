<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

use app\exceptions\Handler;
use paper\providers\DbServiceProvider;
use paper\providers\HttpServiceProvider;
use paper\providers\UrlServiceProvider;
use paper\providers\ViewServiceProvider;

return [

    // +----------------------------------------------------------------------
    // | 基本设置
    // +----------------------------------------------------------------------

    //启动开启debug模式
    'debug'                  => true,

    //显示底部统计信息
    'trace'                  => ($_SERVER['HTTP_HOST'] == '127.2.2.23'),

    //url mode 0 默认模式（pathinfo），1伪静态模式
    'url_mode'               => 1,
    //是否使用composer的自动加载（必须已经安装composer）
    'composer_auto_load'     => true,

    //系统时区
    'default_timezone'       => 'Asia/Shanghai',


    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空模块名
    'empty_module'           => '',

    // 控制器后缀
    'use_controller_prefix'  => 'Controller',
    // 操作方法前缀
    'use_action_prefix'      => false,
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,


    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    //URl后缀
    'url_suffix'             => false,


    // +----------------------------------------------------------------------
    // | 异常捕捉
    // +----------------------------------------------------------------------

    'exception_handler' => Handler::class,

    // +----------------------------------------------------------------------
    // | 绑定服务
    // +----------------------------------------------------------------------

    'providers' => [
        HttpServiceProvider::class,
        DbServiceProvider::class,
        UrlServiceProvider::class,
        ViewServiceProvider::class,
    ],
];