<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/7/8
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\command;

class Build
{

    /**
     * 初始化目录
     */
    private function create()
    {
        $app_path = ROOT . DIRECTORY_SEPARATOR . APP;
        if (!is_dir($app_path)) {
            mkdir($app_path, '0777');
            mkdir($app_path . DIRECTORY_SEPARATOR . 'models', '0777');
            mkdir($app_path . DIRECTORY_SEPARATOR . 'modules', '0777');
            mkdir($app_path . DIRECTORY_SEPARATOR . 'models', '0777');;
        }

        $config_path = ROOT . DIRECTORY_SEPARATOR . 'config';
        if (!is_dir($config_path)) {
            mkdir($config_path, '0777');
            mkdir($config_path . DIRECTORY_SEPARATOR . 'models', '0777');
            mkdir($config_path . DIRECTORY_SEPARATOR . 'modules', '0777');
            mkdir($config_path . DIRECTORY_SEPARATOR . 'models', '0777');;
        }
    }

    public function exec($params)
    {
        $this->create();
    }
}