<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper;


use paper\tplLibs\Engine;

class View
{

    private $engine = null;
    private $template = '';
    private $request = null;

    public function __construct(App $app)
    {
        $this->engine  = new Engine();
        $this->request = $app->request;
    }

    /**
     * @param $var
     * @param $value
     */
    public function assign($var, $value)
    {
        $this->engine->set($var, $value);
    }

    public function view($template = null)
    {
        $this->template = $template;
    }

    /**
     * @param $template
     * @return false|string
     * @throws Exception
     */
    public function fetch()
    {
        ob_start(); //开启缓冲
        ob_implicit_flush(0);
        $this->engine->load($this->template); //运行模板
        $content = ob_get_contents(); //获取输出内容
        ob_end_clean();
        return $content;
    }

    /**
     * @param $template
     * @throws Exception
     */
    public function display()
    {
        $this->engine->load($this->template);
    }

}