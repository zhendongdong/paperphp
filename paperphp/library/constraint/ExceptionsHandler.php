<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\constraint;


interface ExceptionsHandler
{
    public function report($exception);

    public function render($exception);
}