<?php
return [
    //数据库主机地址
    'hostname'             => 'localhost',
    //设置端口
    'port'                 => '3306',
    //设置字符集
    'charset'              => 'utf8mb4',
    //数据库连接用户名
    'username'             => 'root',
    //数据库连接密码
    'password'             => 'zhenxudong',
    //数据库名称
    'database'             => 'test',
    // 数据表前缀
    'prefix'               => 'p_',
    //是否开启sql日志
    'sql_log'              => false,
    //自动完成字段
    'automatic_completion' => true,
    'result_set_type'      => 'collection',
];