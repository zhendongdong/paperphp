<?php
/**
 * Created by ephp
 * User: 22071
 * Date: 2019/6/14
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper;


class Paginate
{

    protected static $config = [];

    private $rows = 0;
    private $pageRows = 20;
    protected $pages = 0;
    private $request = null;
    private $pageColNumber = 8;

    protected $showBeforePage = false;
    protected $beforeStartPage = 0;
    protected $beforeEndPage = 0;

    protected $showAfterPage = false;
    protected $afterStartPage = 0;
    protected $afterEndPage = 0;

    protected $showMiddlePage = true;
    protected $middleStartPage = 0;
    protected $middleEndPage = 0;
    protected $current = 1;
    protected $page = 'page';
    protected $params = [];

    public $data;


    private function __construct($options)
    {
        $this->request = App::getInstance()->request;

        $this->rows     = $options['rows'] ?? 0;
        $this->pageRows = $options['pageRows'] ?? 20;
        $this->page     = $options['page'] ?? 'page';
        $this->params   = $options['params'] ?? $this->request->get();


        $current_page = $this->request->get("page", 1);

        $this->pages = ceil($this->rows / $this->pageRows);

        // var_dump($this->rows);

        if ($current_page > $this->pages) {
            $current_page = $this->pages;
        }
        if ($current_page < 1) {
            $current_page = 1;
        }
        $this->current = $current_page;
    }

    /**
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * @param $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    public static function container($option = [])
    {
        if (!self::$config) {
            self::$config = App::getInstance()->config->paginate();
        }
        $class = self::$config["render"];
        return new $class($option);
    }

    public function getStart()
    {
        return ($this->current - 1) * $this->pageRows;
    }


    public function render()
    {
        //显示页码
        if ($this->pages <= 1) {
            return '';
        }
        if ($this->pages <= $this->pageColNumber) {
            $this->showMiddlePage  = true;
            $this->middleStartPage = 1;
            $this->middleEndPage   = $this->pages;;
        } else {
            $this->pageColNumber   -= 2;
            $this->middleStartPage = $this->current - $this->pageColNumber / 2;
            $this->middleStartPage = $this->middleStartPage >= 1 ? $this->middleStartPage : 1;
            $this->middleEndPage   = $this->middleStartPage + $this->pageColNumber - 1;

            if ($this->middleEndPage >= $this->pages) {
                $this->middleStartPage = $this->pages - $this->pageColNumber;
                $this->middleEndPage   = $this->pages;
            }
            if ($this->pages - $this->middleEndPage > 2) {
                $this->showAfterPage  = true;
                $this->afterStartPage = $this->pages - 2;
                $this->afterEndPage   = $this->pages - 1;
            }
            if ($this->middleStartPage - 1 > 2) {
                $this->showBeforePage  = true;
                $this->beforeStartPage = 1;
                $this->beforeEndPage   = 2;
            }
        }
        return $this->buildPage();
    }


    protected function url($page)
    {
        $this->params['page'] = $page;
        return url('', $this->params);
    }

    protected function buildPage()
    {
        return '';
    }

    /**
     * @return int
     */
    public function getPageRows(): int
    {
        return $this->pageRows;
    }

    /**
     * @param int $pageRows
     */
    public function setPageRows(int $pageRows)
    {
        $this->pageRows = $pageRows;
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @param int $rows
     */
    public function setRows(int $rows)
    {
        $this->rows = $rows;
    }

    public function __toString()
    {
        return $this->render();
    }
}