<?php
/**
 * Created by paperphp
 * User: 22071
 * Date: 2019/9/19
 * Email: <zhendongdong@foxmail.com>
 */

namespace paper\providers;


use paper\App;
use paper\constraint\ExceptionsHandler;
use paper\exception\ExceptionsHandlerRegister;
use paper\exception\Handler;
use paper\support\ServiceProvider;

class ExceptionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ExceptionsHandlerRegister::class, function (App $app) {
            return new ExceptionsHandlerRegister($app);
        });
        $this->app->singleton(ExceptionsHandler::class, function (App $app) {
            $handler = $this->app->config->app('exception_handler') ?: Handler::class;
            return new $handler($app);
        });
    }
}